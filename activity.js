// / It allows us to access methods and functions in easily creating our server.
const express = require("express");

// // We create an application using express.
// app is our server.
const app = express();

const port = 3000;

// Middleware
app.use(express.json()); //Allow your app to read json data
app.use(express.urlencoded({extended: true})); //Allows your app to read data from forms.

// [SECTION] Routes
// if(request.url == "/login" && request.method = "POST") <- using node.js
// This route expects to receive a GET request at the base URI "/".


// Mini activity: Create a "/hello" route that will send a response "Hello from the /hello endpoint" upon sending a GET request.
app.get("/hello", (req, res)=>{
        res.send("Hello from the /hello endpoint");
})

// Upon sending a POST request in the /hello endpoint, the server will response with the message: "Hello there <firstName> <lastName>"
app.post("/hello", (req, res)=>{

        console.log(req.body);
        res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
})

let users = [];

app.post("/signup", (req, res) => {
        if (req.body.userName != "" && req.body.password != "") {
                res.send(`User ${req.body.userName}, successfully registered`);     
                users.push (req.body);
                console.log(users);

        }
        else{
               res.send("Please input username and password");
        }
})
app.get("/users", (req, res) => {
       res.send(users); 

})

app.patch("/change-password", (req, res) => {
        let userExist = false;
       for(i = 0; i < users.length; i++){
         if(users[i].userName == req.body.userName){
                res.send(`User ${req.body.userName}'s password has been updated`);
                users[i].password = req.body.password;
                userExist = true;
                break;
         }
       }

        if (!userExist)
                res.send("Users does not exist")

})








// ACTIVITY BELOW





/*              5. "/home" route
                        - This will send a response "Welcome to the homepage" upon accessing by the client.
*/
app.get("/home", (req, res) =>{
        // res.writehead => res.write => res.end
        res.send("Welcome to the homepage"); 
})




/*              4. "/delete-user" route
                        - This endpoint will delete a user from the mock database, upon sending a "username" as a request.
                        - Before performing any actions in this endpoint, Make sure that the database is not empty and 
                        the user to be deleted exists.
                        - If the mock database is not empty, create a condition that will check if the user exist in
                         the database and will peform the following actions:
                                - If the username is found in the database, remove the user and send a 
                                response "User <username> has been deleted.".
                                - If the user is not found in the database, send a response "User doesn't exists".
                        - if the mock database is empty, send a response "The user database is empty!"
*/

app.delete("/delete-user", (req, res) => {
       if (users.length > 0){
        let userExist = false;
        for(i = 0; i < users.length; i++){

               if (users[i].userName == req.body.userName) {
                        res.send(`User ${users[i].userName} has been deleted`);    
                        console.log(users[i].userName); 
                        users.splice(i, 1);
                        break;

                }
        }
        if (!userExist)
                res.send("User doesn't exists")
       }
       else {
        res.send("The user database is empty!")
       }

})



app.listen(port, () => console.log(`Server running at port ${port}`))